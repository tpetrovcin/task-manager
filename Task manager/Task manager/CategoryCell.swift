import UIKit

class CategoryCell: UITableViewCell {

	@IBOutlet weak var categoryName: UIButton!
	@IBOutlet weak var colorButton: UIButton!
	@IBOutlet weak var colorSetting: UIView!
	@IBOutlet weak var categoryNameEdit: UITextField!

	var parent:Categories?
	var index:Int?

	override func awakeFromNib() {
		super.awakeFromNib()

	}

	@IBAction func changeColor(sender: UIButton) {
		colorSetting.userInteractionEnabled = true
		colorSetting.hidden = false
		colorButton.hidden = true
		categoryName.hidden = true
	}

	@IBAction func editCategoryName(sender: AnyObject) {
		colorButton.hidden = true
		categoryName.hidden = true
		categoryNameEdit.hidden = false
		categoryNameEdit.enabled = true
		categoryNameEdit.becomeFirstResponder()
		categoryNameEdit.text = categoryName.titleLabel?.text
	}

	@IBAction func saveEditedName(sender: UITextField) {
		colorButton.hidden = false
		categoryName.hidden = false
		categoryNameEdit.hidden = true
		categoryNameEdit.enabled = false
		categoryNameEdit.resignFirstResponder()
		if categoryNameEdit.text != nil && categoryNameEdit.text != "" {
			categoryName.setTitle(categoryNameEdit.text, forState: .Normal)
			parent?.categories[index!].setValue(categoryNameEdit.text, forKey: "name")
		}
	}

	@IBAction func colorTouch(sender: UIButton) {
		let color = sender.backgroundColor
		colorButton.backgroundColor = color
		colorSetting.hidden = true
		colorSetting.userInteractionEnabled = false
		colorButton.hidden = false
		categoryName.hidden = false

		let colorData = NSKeyedArchiver.archivedDataWithRootObject(color!)
		parent?.categories[index!].setValue(colorData, forKey: "color")

		print("color \(color)")
	}
}

extension UIView {
	@IBInspectable var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
			layer.borderColor = UIColor.lightGrayColor().CGColor
			layer.borderWidth = 1
		}
	}
}