import UIKit

class Settings: UITableViewController {

	@IBOutlet weak var sortSwitch: UISwitch!

	let defaults = NSUserDefaults.standardUserDefaults()

	override func viewWillAppear(animated: Bool) {
		sortSwitch.on = defaults.boolForKey("sorting")
	}

	@IBAction func sortSwitchChanged(sender: UISwitch) {
		defaults.setBool(sender.on, forKey: "sorting")
	}

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

		if indexPath.row == 2 {
			UIApplication.sharedApplication().cancelAllLocalNotifications()
		}
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}

	@IBAction func doneAction(sender: UIBarButtonItem) {
		performSegueWithIdentifier("unwindToTask", sender: nil)
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "unwindToTask" {
			let taskTable = segue.destinationViewController as? TaskTableViewController
			taskTable?.fetchCoreData()
		}
	}

	@IBAction func unwindToSettings(sender: UIStoryboardSegue) {}
}