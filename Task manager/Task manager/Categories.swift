import UIKit
import CoreData

class Categories: UITableViewController {
	var categories = [NSManagedObject]()
	let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

	var mode = ""

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

		if mode == "select" {
			navigationItem.rightBarButtonItems = nil
			navigationItem.leftBarButtonItems = nil
			self.title = "Select category"
			tableView.allowsSelection = true
		} else {
			self.title = "Categories"
		}

		let managedContext = appDelegate.managedObjectContext
		let categoryFetchRequest = NSFetchRequest(entityName: "Category")

		do {
			let categoryResults = try managedContext.executeFetchRequest(categoryFetchRequest)
			categories = categoryResults as! [NSManagedObject]
			if categories.isEmpty {
				addCategory("To do list", color: UIColor(red: 0.4, green: 0.4, blue: 1, alpha: 1))
				addCategory("Shoping list", color: UIColor(red: 0.4, green: 1, blue: 0.4, alpha: 1))
				addCategory("Important", color: UIColor(red: 1, green: 0.4, blue: 0.4, alpha: 1))
				addCategory("Wishlist", color: UIColor(red: 1, green: 0.4, blue: 1, alpha: 1))
				save()
			}
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "unwindToDetail" {
			let detailViewController = segue.destinationViewController as? DetailViewController
			let categoryIndex = tableView.indexPathForSelectedRow?.row
			let colorData = categories[categoryIndex!].valueForKey("color") as! NSData
			let color = NSKeyedUnarchiver.unarchiveObjectWithData(colorData) as! UIColor
			detailViewController?.categoryButton.backgroundColor = color
			detailViewController?.categoryButton.setTitle(categories[categoryIndex!].valueForKey("name") as? String, forState: .Normal)
			detailViewController?.categoryIndex = categoryIndex
		}
	}

	@IBAction func addButton(sender: UIBarButtonItem) {
		addCategory("New category", color: UIColor.clearColor())
		tableView.reloadData()
	}

	func addCategory(name:String, color:UIColor) {
		let managedContext = appDelegate.managedObjectContext
		let entity =  NSEntityDescription.entityForName("Category", inManagedObjectContext:managedContext)

		let category = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)

		category.setValue(name, forKey: "name")
		let colorData:NSData = NSKeyedArchiver.archivedDataWithRootObject(color)
		category.setValue(colorData, forKey: "color")


		categories.append(category)
	}

	func save() {
		let managedContext = appDelegate.managedObjectContext
		do {
			try managedContext.save()
		} catch {
			print("Could not save \(error)")
		}
	}

	@IBAction func saveAction(sender: AnyObject?) {
		save()
		performSegueWithIdentifier("unwindToSettings", sender: sender)
	}

	@IBAction func cancelAction(sender: UIBarButtonItem) {
		performSegueWithIdentifier("unwindToSettings", sender: sender)
	}

	// MARK: Table view functions

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		print("categories count: \(categories.count)")
		return categories.count
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		let cellIdentifier = "categoryCell"
		let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CategoryCell

		let category = categories[indexPath.row]

		print(category.valueForKey("name")!)

		cell.categoryName.setTitle(category.valueForKey("name") as? String, forState: .Normal)

		let categoryColorData = category.valueForKey("color") as! NSData

		cell.colorButton.backgroundColor = NSKeyedUnarchiver.unarchiveObjectWithData(categoryColorData) as? UIColor

		cell.index = indexPath.row
		cell.parent = self

		if mode == "select" {
			cell.contentView.userInteractionEnabled = false
		}

		return cell
	}

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		performSegueWithIdentifier("unwindToDetail", sender: nil)
	}
}