import UIKit
import CoreData

class TaskTableViewController: UITableViewController {

	var tasks = [NSManagedObject]()
	var completedTasks = [NSManagedObject]()
	var categories = [NSManagedObject]()

	let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

	override func viewDidLoad() {
		super.viewDidLoad()

		fetchCoreData()
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		let managedContext = appDelegate.managedObjectContext
		let categoryFetchRequest = NSFetchRequest(entityName: "Category")
		do {
			let categoryResults = try managedContext.executeFetchRequest(categoryFetchRequest)
			categories = categoryResults as! [NSManagedObject]
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}

		tableView.reloadData()
	}

	func fetchCoreData() {
		let managedContext = appDelegate.managedObjectContext

		let taskFetchRequest = NSFetchRequest(entityName: "Task")
		let completedFetchRequest = NSFetchRequest(entityName: "Task")
		let categoryFetchRequest = NSFetchRequest(entityName: "Category")

		let defaults = NSUserDefaults.standardUserDefaults()
		let bool = defaults.boolForKey("sorting") ?? true
		let key = bool ? "date" : "name"

		let sort = NSSortDescriptor(key: key, ascending: true)

		taskFetchRequest.predicate = NSPredicate(format: "completed = %@", false)
		taskFetchRequest.sortDescriptors = [sort]
		completedFetchRequest.predicate = NSPredicate(format: "completed = %@", true)

		do {
			let taskResults = try managedContext.executeFetchRequest(taskFetchRequest)
			tasks = taskResults as! [NSManagedObject]

			let completedResults = try managedContext.executeFetchRequest(completedFetchRequest)
			completedTasks = completedResults as! [NSManagedObject]

			let categoryResults = try managedContext.executeFetchRequest(categoryFetchRequest)
			categories = categoryResults as! [NSManagedObject]
		} catch let error as NSError {
			print("Could not fetch \(error), \(error.userInfo)")
		}
	}

	func setTask(name: String, category: Int, date: NSDate, completed:Bool, position:Int?) {

		if position != nil {
			tasks[position!].setValue(name, forKey: "name")
			tasks[position!].setValue(category, forKey: "category")
			tasks[position!].setValue(date, forKey: "date")
			tasks[position!].setValue(completed, forKey: "completed")

			if completed {
				completedTasks.append(tasks[position!])
				tasks.removeAtIndex(position!)
			}
		} else {
			let managedContext = appDelegate.managedObjectContext
			let entity =  NSEntityDescription.entityForName("Task", inManagedObjectContext:managedContext)
			let task = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
			task.setValue(name, forKey: "name")
			task.setValue(category, forKey: "category")
			task.setValue(date, forKey: "date")
			task.setValue(completed, forKey: "completed")
			if completed {
				completedTasks.insert(task, atIndex: 0)
			} else {
				tasks.insert(task, atIndex: 0)
			}
		}

		tableView.reloadData()
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "editTask" {
			let navController = segue.destinationViewController as! UINavigationController
			let detailView = navController.childViewControllers[0] as! DetailViewController

			let selectedCell = sender as! TaskCell
			print("edit")

			print("prepare")
			let indexPath = tableView.indexPathForCell(selectedCell)
			let selectedItem = tasks[indexPath!.row]
			detailView.task = selectedItem
			detailView.categories = categories
			detailView.position = indexPath!.row
		}
	}

	@IBAction func unwindToTaskTable(sender: UIStoryboardSegue) {
	}

	// MARK: Table view functions

	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

		var numberOfSections = 1

		if completedTasks.count > 0 {
			numberOfSections += 1
		}
		print("number of sections: \(numberOfSections)")
		return numberOfSections
	}

	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

		var titleForHeader = ""

		switch section {
		case 0:
			titleForHeader = "Tasks"
			break
		case 1:
			titleForHeader = "Completed"
			break
		default:
			titleForHeader = ""
		}
		return titleForHeader
	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		var numberOfRows = 0

		switch section {
		case 0:
			numberOfRows = tasks.count > 0  ?  tasks.count : 1
			break
		case 1:
			numberOfRows = completedTasks.count
			break
		default:
			numberOfRows = 0
		}
		print("number of rows in section \(section): \(numberOfRows)")
		return numberOfRows
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		var task:NSManagedObject?
		print("section: \(indexPath.section), tasks: \(tasks.count)")

		if (indexPath.section == 0 && tasks.count > 0) || indexPath.section == 1 {
			let cellIdentifier = "taskCell"
			let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TaskCell

			switch indexPath.section {
			case 0:
				task = tasks[indexPath.row]
				cell.selectionStyle = .Default
				cell.contentView.layer.opacity = 1
			case 1:
				task = completedTasks[indexPath.row]
				cell.selectionStyle = .None
				cell.contentView.layer.opacity = 0.3
			default:
				return cell
			}

			cell.nameLabel.text = task!.valueForKey("name") as? String

			let date = task!.valueForKey("date") as? NSDate

			let dateFormatter = NSDateFormatter()
			dateFormatter.dateFormat = "dd.MM.YYYY\n HH:mm"
			cell.deadlineLabel.text = dateFormatter.stringFromDate(date!)

			let categoryIndex = task!.valueForKey("category") as! Int
			let categoryName = " \(categories[categoryIndex].valueForKey("name") as! String) "
			let categoryColorData = categories[categoryIndex].valueForKey("color") as! NSData

			let categoryColor:UIColor = NSKeyedUnarchiver.unarchiveObjectWithData(categoryColorData) as! UIColor

			cell.categoryLabel.text = categoryName
			cell.categoryLabel.layer.cornerRadius = 5
			cell.categoryLabel.layer.masksToBounds = true
			cell.categoryLabel.backgroundColor = categoryColor

			return cell

		} else {
			let cellIdentifier = "newTaskCell"
			let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
			return cell
		}
	}

	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if indexPath.section == 0 && tasks.count == 0 {
			return false
		}
		return true
	}

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if tasks.count > 0 && indexPath.section == 0 {
			performSegueWithIdentifier("editTask", sender: tableView.cellForRowAtIndexPath(indexPath))
		}
	}

	override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {

		let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext

		let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in

			switch indexPath.section {
			case 0:
				managedContext.deleteObject(self.tasks[indexPath.row] as NSManagedObject)
				self.tasks.removeAtIndex(indexPath.row)

				break
			case 1:
				managedContext.deleteObject(self.completedTasks[indexPath.row] as NSManagedObject)
				self.completedTasks.removeAtIndex(indexPath.row)
			default:
				break
			}

			do {
				try managedContext.save()
			} catch {
				print(error)
			}

			if tableView.numberOfRowsInSection(indexPath.section) == 1 {
				if indexPath.section == 0 {
					self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
				} else {
					self.tableView.deleteSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
				}
			} else {
				self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			}
		}
		deleteAction.backgroundColor = UIColor.redColor()

		let doneAction = UITableViewRowAction(style: .Normal, title: "Done") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
			let task = self.tasks[indexPath.row]
			task.setValue(true, forKey: "completed")

			do {
				try managedContext.save()
			} catch {
				print(error)
			}

			self.tasks.removeAtIndex(indexPath.row)

			if self.tasks.count == 0 {
				self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			} else {
				self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			}

			self.completedTasks.insert(task, atIndex: 0)

			if self.completedTasks.count == 1 {
				self.tableView.insertSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
			} else {
				self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow:0 , inSection: 1)], withRowAnimation: .Automatic)
			}

			tableView.setEditing(false, animated: true)
		}
		doneAction.backgroundColor = UIColor.greenColor()

		let undoneAction = UITableViewRowAction(style: .Normal, title: "Undone") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
			let task = self.completedTasks[indexPath.row]
			task.setValue(false, forKey: "completed")

			do {
				try managedContext.save()
			} catch {
				print(error)
			}


			self.completedTasks.removeAtIndex(indexPath.row)

			if self.completedTasks.count == 0 {
				self.tableView.deleteSections(NSIndexSet(index: 1), withRowAnimation: .Automatic)
			} else {
				self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			}

			self.tasks.insert(task, atIndex: 0)

			if self.tasks.count == 1{
				self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow:0 , inSection: 0)], withRowAnimation: .Automatic)
			} else {
				self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow:0 , inSection: 0)], withRowAnimation: .Automatic)
			}

			tableView.setEditing(false, animated: true)
		}
		undoneAction.backgroundColor = UIColor.blueColor()

		if indexPath.section == 0 {
			return [deleteAction, doneAction]
		} else {
			return [deleteAction, undoneAction]
		}
	}
}

