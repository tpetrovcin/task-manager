import UIKit

class TaskCell: UITableViewCell {

	@IBOutlet weak var categoryLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var deadlineLabel: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
	}
}