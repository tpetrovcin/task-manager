import UIKit
import CoreData

class DetailViewController: UIViewController, UITextFieldDelegate {

	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var datePicker: UIDatePicker!
	@IBOutlet weak var categoryButton: UIButton!
	@IBOutlet weak var reminderSwitch: UISwitch!
	@IBOutlet weak var saveButton: UIBarButtonItem!
	@IBOutlet weak var completedSwitch: UISwitch!

	var task:NSManagedObject?
	var categories = [NSManagedObject]()
	var categoryIndex:Int?

	var position:Int?
	var notificationID:String?

	let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

	private let defaultName = "Click to edit task name"

	override func viewDidLoad() {
		super.viewDidLoad()

		print(task)
		if task != nil {

			let taskName = task!.valueForKey("name") as? String
			categoryIndex = task!.valueForKey("category") as? Int
			let date = task!.valueForKey("date") as! NSDate

			nameLabel.text = taskName

			let category = categories[categoryIndex!]
			let color = NSKeyedUnarchiver.unarchiveObjectWithData(category.valueForKey("color") as! NSData) as? UIColor
			categoryButton.backgroundColor = color
			categoryButton.setTitle(category.valueForKey("name") as? String, forState: .Normal)

			datePicker.setDate(date, animated: true)

			notificationID = "\(taskName!)\(categoryIndex!)\(date)"
		} else {
			nameLabel.hidden = true
			nameTextField.hidden = false
			nameTextField.enabled = true
		}

		datePicker.minimumDate = NSDate()
	}

	@IBAction func saveAction(sender: UIBarButtonItem) {
		if nameLabel.text == nil || nameLabel.text == "" {
			viewAlertWithString("Please enter valid task name")
		} else if categoryIndex == nil {
			viewAlertWithString("Please select category")
		} else {
			performSegueWithIdentifier("unwindToTaskTable", sender: sender)
		}
	}

	@IBAction func cancelAction(sender: UIBarButtonItem) {
		performSegueWithIdentifier("unwindToTaskTable", sender: nil)
	}

	func viewAlertWithString(string:String) {
		let alertController = UIAlertController(title: "", message:	string, preferredStyle: UIAlertControllerStyle.Alert)
		alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))

		self.presentViewController(alertController, animated: true, completion: nil)
	}
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "selectCategory" {
			let navController = segue.destinationViewController as! UINavigationController
			let categories = navController.childViewControllers[0] as! Categories
			categories.mode = "select"
		} else if segue.identifier! == "unwindToTaskTable" && sender != nil {

			let taskTable = segue.destinationViewController as! TaskTableViewController

			taskTable.setTask(nameLabel.text!, category: categoryIndex!, date: datePicker.date, completed: completedSwitch.on, position: position)

			if notificationID != nil {
				print("searching for ID: \(notificationID)")
				let application = UIApplication.sharedApplication()
				let scheduledNotifications = application.scheduledLocalNotifications!

				for notification in scheduledNotifications {
					if let nID = notification.userInfo?["NotificationID"] as? String {
						if nID == notificationID {
							print("id match")
							application.cancelLocalNotification(notification)
							break
						}
					}
				}
			}

			if reminderSwitch.on {
				notificationID = "\(nameLabel.text!)\(categoryIndex!)\(datePicker.date)"
				print(notificationID)

				let notification = UILocalNotification()
				notification.alertBody = nameLabel.text
				notification.alertAction = "open"
				notification.fireDate = datePicker.date
				notification.userInfo = ["NotificationID": notificationID!]
				UIApplication.sharedApplication().scheduleLocalNotification(notification)
			}
		}
	}

	@IBAction func unwindToDetail(sender: UIStoryboardSegue) {
	}

	@IBAction func nameLabelTouch(sender: AnyObject) {
		print("label touch")
		nameTextField.text = nameLabel.text
		setNameTextFieldVisibility(true)
		nameTextField.becomeFirstResponder()
	}

	func textFieldShouldReturn(textField: UITextField) -> Bool {
		nameLabel.text = nameTextField.text
		setNameTextFieldVisibility(false)
		textField.resignFirstResponder()
		return false
	}

	func setNameTextFieldVisibility(bool:Bool) {
		nameLabel.hidden = bool
		nameLabel.enabled = !bool
		nameTextField.hidden = !bool
		nameTextField.enabled = bool
	}
}
